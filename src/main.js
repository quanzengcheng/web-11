import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/store'
import './styles/iconfont.css'
import './styles/reset.css'

// 创建全局时间过滤器
Vue.filter('fmtTime', function (timeStr) {
  let timeObj = new Date(timeStr)
  let y = timeObj.getFullYear()
  let m = timeObj.getMonth() + 1
  let d = timeObj.getDate()
  return y + '/' + m + '/' + d
})
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
