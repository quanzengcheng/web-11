
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:8888/api'
// 获取新闻列表
export const newList = () => axios.get('/getnewslist').then(res => res.data)

// 获取新闻详情页面
export const newDel = id => axios.get(`/getnew/${id}`).then(res => res.data)
// 获取评论列表
export const CommentList = (id, pageindex) => axios.get(`/getcomments/${id}?pageindex=${pageindex}`).then(res => res.data)
// 发表评论
export const Comments = (id, content) => axios.post(`/postcomment/${id}`, content).then(res => res.data)

// 获取图片列表分类**/getimgcategory**
export const imgLists = () => axios.get('/getimgcategory').then(res => res.data)
// 获取图片列表图片**/getimgcategory**
export const Imgtable = id => axios.get(`/getimages/${id}`).then(res => res.data)
//* */getthumimages/:id**  获取图片详情
export const Imgdetails = id => axios.get(`/getthumimages/${id}`).then(res => res.data)

// 获取图片描述
export const ImgMiaosu = id => axios.get(`/getimageInfo/${id}`).then(res => res.data)
// 获取商品列表
export const getGoodsList = pageindex => axios.get(`/getgoods?pageindex=${pageindex}`).then(res => res.data)
// 获取商品详情
export const getGoodsDetail = id => axios.get(`/goods/getinfo/${id}`).then(res => res.data)
// 获取缩略图
export const getImg = id => axios.get(`/getthumimages/${id}`).then(res => res.data)
// 获取商品图文介绍
export const getImgDesc = id => axios.get(`/goods/getdesc/${id}`).then(res => res.data)
// 获取购物车数据
export const getCartInfo = id => axios.get(`/goods/getshopcarlist/${id}`).then(res => res.data)
