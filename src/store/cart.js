const state = {
  num: 0
}
const mutations = {
  getNum: state => {
    let total = 0
    let cartList = JSON.parse(localStorage.getItem('cartList') || '[]')
    cartList.map(item => {
      total += item.num
    })
    state.num = total
  },
  addNum: state => {
    state.num++
  }
}
export default {
  state,
  mutations
}
